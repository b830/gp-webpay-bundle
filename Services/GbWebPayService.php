<?php


namespace App\Akip\GpWebpayBundle\Services;


use App\Akip\EshopBundle\Entity\Customer;
use App\Akip\EshopBundle\Entity\Order;
use App\Akip\EshopBundle\Entity\Payment;
use App\Akip\GpWebpayBundle\Entity\WebPayRequest;
use App\Akip\GpWebpayBundle\Entity\WebPayResponse;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Email;

class GbWebPayService
{
    const CURRENCY_ISO_CODE = 203;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * ProductController constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function createPayment($idOrder, $idPayment, $summary, $returnRequestData = false)
    {
        $onlinePayment = $this->em->getRepository(Payment::class)->findOneBy(['slug' => Payment::ONLINE]);
        $merchantNumber = $onlinePayment->getOptions()['merchant-number']['value'];
        $responseUrl = "{$_ENV['PROTOCOL']}://{$_ENV['HOST_NAME']}/objednavka/payment-check";


        if (!extension_loaded('openssl')) {
            $result['status'] = false;
            $result['reason'] = 'PHP extension OpenSSL is not loaded.';
        } else {

            if (array_key_exists('ok', $_GET) && (int)$_GET['ok'] === 1) {
                $result['reason'] = 'Order payment done, weSmart';
                $result['status'] = true;

            } else {
                $request = new WebPayRequest();
                $request->setPrivateKey($_ENV['PAYMENT_KEY_FILE'], $_ENV['PAYMEMT_KEY_PWD']);

                if ($request->isVerified()) {
                    $request->setWebPayUrl($_ENV['PAYMENT_URL']);
                    $request->setResponseUrl($responseUrl);
                    $request->setMerchantNumber($merchantNumber);
                    $request->setOrderInfo(
                        $idPayment  /* webpay objednávka */,
                        $idOrder /* interní objednávka */,
                        $summary /* cena v CZK */
                    );

                    $result['params'] = $request->getParams();
                    $result['gatewayUrl'] = $request->requestUrl();
                    $result['reason'] = 'Order payment response check, Kkona ';

                    // Create order response
                    if ($summary > 0) {
                        $result['status'] = true;
                        $result['reason'] = 'Order payment done, Kappa ';
                    }
                } else {
                    $result['error'] = $request->getError();
                }
            }

        }
        if ($returnRequestData) {
            return $request->getParams();
        }
        return $request->requestUrl();
    }

    public function checkPayment($data)
    {

        if (!extension_loaded('openssl')) {
            $result['status'] = false;
            $result['reason'] = 'PHP extension OpenSSL is not loaded.';
        } else {

            $response = new WebPayResponse();
            $response->setPublicKey($_ENV['GPE_PUBLIC_KEY_FILE']);
            $response->setresponseParams($data);
            $result['params'] = $data;
            $result['digest'] = $response->getDigest();
            $result['digest1'] = $response->getDigest(true);
            $result['keyVerified'] = $response->isVerified();
            $result['keyVerified1'] = $response->isVerified(true);
            $result['status'] = $response->verify();

            if ($result['status'] === true) {
                $result['reason'] = 'Order payment response success, weSmart';
                $result['status'] = true;
            }
        }

        return $result;
    }
}
