<?php

namespace App\Akip\GpWebpayBundle\Entity;


class WebPayResponse {
    private $publicKey,
            $responseParams= [],
            $digest, 
            $digest1;

    public $keyVerified = 0,
            $keyVerified1 = 0;

    public function __contruct() {

    }

    public function setPublicKey($file) {
        $fp = fopen($file, "r");
        $key = fread($fp, filesize ($file));
        fclose($fp);
        if (!($this->publicKey = openssl_get_publickey($key))) {
            echo "'$file' is not valid PEM public key (or passphrase is incorrect).";
            die;
        }
    }

    public function setresponseParams($params) {
        $this->responseParams['OPERATION'] = isset ($params['OPERATION']) ? $params['OPERATION'] : '';
        $this->responseParams['ORDERNUMBER'] = isset ($params['ORDERNUMBER']) ? $params['ORDERNUMBER'] : '';
        $this->responseParams['MERORDERNUM'] = isset ($params['MERORDERNUM']) ? $params['MERORDERNUM'] : '';
        //$this->responseParams['MD'] = isset ($params['MD']) ? $params['MD'] : '';
        $this->responseParams['PRCODE'] = isset ($params['PRCODE']) ? $params['PRCODE'] : '';
        $this->responseParams['SRCODE'] = isset ($params['SRCODE']) ? $params['SRCODE'] : '';
        $this->responseParams['RESULTTEXT'] = isset ($params['RESULTTEXT']) ? $params['RESULTTEXT'] : '';

        $this->digest = isset ($params['DIGEST']) ? $params['DIGEST'] : '';
        $this->digest1 = isset ($params['DIGEST1']) ? $params['DIGEST1'] : '';
    }

    public function getDigest($next = false) {
        return $next ? $this->digest1 : $this->digest;
    }

    public function isVerified($next = false) {
        return $next ? $this->keyVerified1 : $this->keyVerified;
    }

    public function verify() {
        $data = implode('|', $this->responseParams);
        $digest = base64_decode($this->digest);
        $digest1 = base64_decode($this->digest1);
        $this->keyVerified = openssl_verify($data, $digest, $this->publicKey);
        $this->keyVerified1 = openssl_verify($data, $digest1, $this->publicKey);
        return (($this->keyVerified == 1 || $this->keyVerified1 == 1) && ($this->responseParams['PRCODE'] == 0) && ($this->responseParams['SRCODE'] == 0)) ? true : false;
    }

    public function orderWebpay() {return $this->responseParams['ORDERNUMBER'];}
    public function orderMerchant() {return $this->responseParams['MERORDERNUM'];}
}
