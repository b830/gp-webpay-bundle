<?php
namespace App\Akip\GpWebpayBundle\Entity;

use App\Akip\GpWebpayBundle\Services\GbWebPayService;

class WebPayRequest {

    private $privateKey,
    $webPayUrl,
    $responseUrl,
    $merchantNumber,
    $webPayOrder,
    $merchantOrder,
    $amount,
    $keyVerified,
    $error;

    public function __contruct() {
        $this->error = '';
        $this->keyVerified = false;
    }

    public function setPrivateKey($file, $passphrase) {
        $fp = fopen ($file, "r");
        $key = fread ($fp, filesize($file));
        fclose ($fp);
        if (!($this->privateKey = openssl_pkey_get_private($key, $passphrase))) {
            throw new \Exception("'$file' is not valid PEM private key (or passphrase is incorrect).");
            $this->error = "'$file' is not valid PEM private key (or passphrase is incorrect).";
        }else{
            $this->keyVerified = true;
        }
    }

    public function getError() {
        return $this->error;
    }

    public function isVerified() {
        return $this->keyVerified;
    }

    public function setOrderInfo($webPayOrder, $merchantOrder, $price) {
        $this->webPayOrder = $webPayOrder;
        $this->merchantOrder = $merchantOrder;
        $this->amount = $price * 100;
    }

    public function setWebPayUrl($url) {
        $this->webPayUrl = $url;
    }

    public function setResponseUrl($responseUrl) {
        $this->responseUrl = $responseUrl;
    }

    public function setMerchantNumber($merchantNumber) {
        $this->merchantNumber = $merchantNumber;
    }

    public function getParams() {
        $params = [];
        $params ['MERCHANTNUMBER'] = $this->merchantNumber;
        $params ['OPERATION'] = 'CREATE_ORDER';
        $params ['ORDERNUMBER'] = $this->webPayOrder;
        $params ['AMOUNT'] = $this->amount;
        $params ['CURRENCY'] = GbWebPayService::CURRENCY_ISO_CODE;
        $params ['DEPOSITFLAG'] = 1;
        $params ['MERORDERNUM'] = $this->merchantOrder;
        //$params ['MD'] = '';
        $params ['URL'] = $this->responseUrl;

        $digestText = implode ('|', $params);
        openssl_sign ($digestText, $signature, $this->privateKey);
        $signature = base64_encode ($signature);
        $params ['DIGEST'] = $signature;

        return $params;
    }

    public function requestUrl () {
        $params = $this->getParams();
        $url = $this->webPayUrl . '?' . http_build_query ($params);
        return $url;
    }
}
