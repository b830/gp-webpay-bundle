<?php

namespace App\Akip\GpWebpayBundle\Controller;

use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use phpDocumentor\Reflection\Element;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class BaseController extends AbstractFOSRestController
{
    const JSON_DATE_FORMAT = 'Y-m-d\TH:i:s.v\Z';

    protected $filter = [];
    protected $order = [];

    public function __construct()
    {

    }

    public function checkLocale($locales)
    {
        $existLocales = explode("|", $_ENV['LOCALES']);
        foreach ($locales as $locale) {
            if (!in_array($locale, $existLocales)) {
                throw new HttpException(Response::HTTP_UNPROCESSABLE_ENTITY, "Locale [{$locale}] is not allowed. Allowed locales are - [{$_ENV['LOCALES']}]");
            }
        }
    }

    public function getConfigSizes()
    {
        $data = explode("|", $_ENV['PICTURE_SIZES']);
        if (empty($data[0]))
            throw new HttpException(Response::HTTP_NOT_IMPLEMENTED, "Missing configuration of PICTURE_SIZES in portainer. Example : 100x100|50x10");
        foreach ($data as $item) {
            $temp = explode("x", $item);
            $result[] = [
                'width' => (int)$temp[0],
                'height' => (int)$temp[1]
            ];
        }
        if (!$result)
            throw new HttpException(Response::HTTP_NOT_IMPLEMENTED, "Missing configuration of PICTURE_SIZES sizes in portainer");
        return $result;
    }

    public function getDefaultSize()
    {
        $data = explode("|", $_ENV['DEFAULT_PICTURE_SIZE']);
        if (empty($data[0]))
            throw new HttpException(Response::HTTP_NOT_IMPLEMENTED, "Missing configuration of DEFAULT_PICTURE_SIZE in portainer. Example : 100x100");
        $temp = explode("x", $data[0]);
        $result = [
            'width' => (int)$temp[0],
            'height' => (int)$temp[1]
        ];
        return $result;
    }

    public function checkFolderName(string $folderName, string $fileName): bool
    {
        if (substr($folderName, 0, 3) === substr($fileName, 0, 3)) {
            return true;
        } else {
            return false;
        }
    }

    public function getLocalesList()
    {
        return $existLocales = explode("|", $_ENV['LOCALES']);
    }


    static function validate($entity, ValidatorInterface $validator)
    {
        $errors = $validator->validate($entity);
        if (count($errors)) {

            $errorMessages = [];
            /**
             * @var $e ConstraintViolation
             */
            foreach ($errors as $e) {
                $errorMessages[$e->getPropertyPath()][] = $e->getMessage();
            }

            return (new JsonResponse($errorMessages, 422));
        }
    }

    /**
     * @param ParamFetcherInterface $pf
     * @param $paramName
     * @return \DateTime|null
     */
    protected function getDateTimeFromParam(ParamFetcherInterface $pf, $paramName)
    {
        $date = \DateTime::createFromFormat(self::JSON_DATE_FORMAT, $pf->get($paramName));
        if ($date === false) {
            return null;
        }
        return $date;
    }

    protected function listResponse($data, $totalResultsCount, $filteredResultsCount, $limit, $offset)
    {
        if (count($data) <= 0)
            $data = array();
        return [
            'meta' => [
                'totalCount' => $totalResultsCount,
                'filteredCount' => $filteredResultsCount,
                'limit' => $limit,
                'offset' => $offset,
            ],
            'results' => $data
        ];
    }


    protected function getOrderBy($clientOrder)
    {
        if (!$clientOrder || !is_array($clientOrder)) {
            return [];
        }

        $order = [];

        foreach ($clientOrder as $co) {
            $order[$co['column']] = strtoupper(isset($co['dir']) ? $co['dir'] : 'asc');
        }

        return $order;
    }

    protected function getFilter($name)
    {
        if (isset($this->filter[$name]) && !empty($this->filter[$name])) {
            return $this->filter[$name];
        }
        return false;
    }

}
